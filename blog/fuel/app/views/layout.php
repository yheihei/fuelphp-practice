<?php echo Html::doctype('html5'); ?>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php echo $header; ?>
        <?php echo $content; ?>
        <?php echo $footer; ?>
    </body>
</html>