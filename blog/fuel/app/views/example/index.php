<h2>FuelPHPの優れた点</h2>
<ul>
    <li>PHP 5.3+ネイティブ</li>
    <li>HMVC</li>
    <li>Autoloading</li>
    <li>Ormパッケージ</li>
    <li>oilコマンドによる開発支援</li>
    <li>MigrationによるDB変更履歴管理</li>
</ul>
<h2>FuelPHPが向いている人</h2>
<ul>
    <li>これからPHPのMVCフレームワークを覚えようという人</li>
    <li>既存の高機能MVCが複雑すぎて学習途中で萎えてしまった人</li>
    <li>CodeIgniterを使っていていまひとつ不満だった人</li>
</ul>
<h3>フルーツの名前取得</h3>
<button id="get_fruits">取得</button>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
$(function() {
    $('#get_fruits').click(function(e) {
        
        $.get("/practice/blog/public/fruit/list.json", function(results){
            //console.log(results);
            $("#fruits").append('<pre>');
            results.forEach(function(fruits){
                console.log(fruits);
                $("#fruits").append('{ name:' + fruits['name']+
                    ', color: ' + fruits['color'] + '},');
            });
            $("#fruits").append('</pre>');
        });
    });
});
</script>

<div id="fruits"></div>
