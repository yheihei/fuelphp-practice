<?php echo Html::doctype('html5'); ?>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1>ユーザー一覧</h1>
        <?php foreach($users as $user) : ?>
        <?php //var_dump($user) ?>
            <table>
                <tr>
                    <td><?php echo $user->id; ?></td>
                    <td><?php echo $user->name; ?></td>
                    <td><?php echo $user->email; ?></td>
                    <td><?php echo $user->sex; ?></td>
                    <td><?php if(count($user->phones) != 0) echo $user->phones[$user->id]->number; ?></td>
                </tr>
            </table>
        <?php endforeach; ?>
    </body>
</html>