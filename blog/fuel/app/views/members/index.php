<!DOCTYPE html>
<html lang="ja">
    <head>
        <title>メンバー一覧</title>
        <meta charset="UTF-8" />
    </head>
    <body>
        <table>
        <?php echo $title ?>
        <?php foreach ($members as $member): ?>
            <tr>
                <td><?php echo sprintf("%05d", $member['id']); ?></td>
                <td><?php echo $member['name']; ?></td>
            </tr>
        <?php endforeach; ?>
        </table>
    </body>
</html>