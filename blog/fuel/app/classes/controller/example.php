<?php

class Controller_Example extends Controller_Template {
    public $template = 'example/template';
    public function action_index() {
        $this->template->title = 'FuelPHPの特徴';
        $this->template->content = View::forge('example/index'); //別Viewの情報を読み込めちゃう
    }
    
    public function action_another() {
        $this->template = View::forge('example/another_template'); //templateを選択してからtitleなどのデータを設定していく。順序が逆になるとデータが空になる。
        $this->template->title = '改訂版FuelPHPの特徴';
        $this->template->content = View::forge('example/index'); //別Viewの情報を読み込めちゃう
    }
    
}