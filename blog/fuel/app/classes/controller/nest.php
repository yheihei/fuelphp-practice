<?php
class Controller_Nest extends Controller {
    public function action_index() {
        $view = View::forge('layout');
        
        $view->header = View::forge('header');
        $view->content = View::forge('content');
        $view->footer = View::forge('footer');
        // set_globalをするとlayoutの子のviewまで変数が渡せる
        $view->set_global('username', 'Yhei');
        $view->set_global('title', 'Home');
        
        return $view;
    } 
}