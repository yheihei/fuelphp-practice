<?php 

class Controller_Products extends Controller {
    public function action_index($code) {
        //商品トップページの処理
        Log::info('action_index!');
        echo $code . ' shouhinnnn!!!!';
        // TODO Viewのforgeでは表示されずコントローラーでhtml出力している。もっとやり方があるはず。
        //return View::forge('products/index');
    }
    public function action_detail($code) {
        //商品詳細ページの処理
        echo $code . ' detail  shouhinnnn!!!!';
        // TODO Viewのforgeでは表示されずコントローラーでhtml出力している。もっとやり方があるはず。
        //return Response::forge(View::forge('products/detail'));
    }
    public function action_photos($code) {
        //商品写真一覧ページの処理
        echo $code . ' photo !!!  shouhinnnn!!!!';
        // TODO Viewのforgeでは表示されずコントローラーでhtml出力している。もっとやり方があるはず。
        //return Response::forge(View::forge('products/photos'));
    }
    
    // /products/商品コード/アクション という特殊なURL呼び出しを振り分けする
    public function router($method_name, $uri_params) {
        Log::info('router!');
        $code = $method_name; //商品コードを保存しておく
        $action = array_shift($uri_params); //第二引数の最初の要素(アクション)を取り出し代入
        Log::info('$code = ' . $code);
        Log::info('$action = ' . $action);
        if (method_exists($this, 'action_' . $action)) { //該当するアクションがあるかどうか
            $method = 'action_' . $action;
            Log::info('$method = ' . $method);
            $this->$method($code);
        } else {
            $this->action_index($code);
        }
    }
}