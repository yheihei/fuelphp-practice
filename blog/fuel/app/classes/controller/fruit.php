<?php
class Controller_Fruit extends Controller_Rest {
    
    //Getメソッドを処理する エンドポイントは/fruit/list
    public function get_list() {
        // formatの指定
        $this->format = 'json';
        $data = array(
            0 => array(
                'name' => 'orange',
                'color' => 'orange'
                ),
            1 => array(
                'name' => 'apple',
                'color' => 'red'
                ),
        );
        return $this->response($data);
    }
    
    //Postメソッドを処理する エンドポイントは/fruit/list
    public function post_list() {
        
    }
    
    
}
