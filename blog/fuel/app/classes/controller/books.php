<?php

class Controller_Books extends Controller_Template {
    public function action_index() {
        $count = Model_Books::count();
        $config = array(
            'pagination_url' => Uri::base(false).'books/index',
            'uri_segment' => 3,
            'num_links' => 4,
            'per_page' => 5,
            'total_items' => $count,
            'show_first' => true,
            'show_last' => true,
        );
        $pagination = Pagination::forge('mypagenation', $config);
        
        $books = Model_Books::query()
        ->rows_offset($pagination->offset)
        ->rows_limit($pagination->per_page)
        ->get();
        
        $this->template->title = '宮沢賢治:作品一覧';
        $view = View::forge('books/index');
        $view->set_safe('pagination', $pagination); //HTMLがエスケープされないようにset_safeでセットすること
        $view->set('books', $books);
        $this->template->content = $view; //template.phpのcontent部分にぶちこむ
    }
}