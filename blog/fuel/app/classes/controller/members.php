<?php
class Controller_Members extends Controller {
    public function action_index() {
        $view = View::forge('members/index');
        
        $view->title = '<h1>メンバーページへようこそ</h1>';
        $view->auto_filter(false); //htmlタグのサニタイズ(「<」が&ltとかになったりする)抑止
        
        $members = array();
        $members[] = array('id' => 1, 'name' => '小久保');
        $members[] = array('id' => 2, 'name' => '中久保');
        $members[] = array('id' => 3, 'name' => '大久保');
        $view->members = $members;
        return $view;
    }
}