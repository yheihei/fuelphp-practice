<?php

class Controller_Users extends Controller {
    public function action_index() {
        //$users = Model_Users::find_all();
        //$view = View::forge('users/index');
        //$view->users = $users;
        //return $view;
        //return Response::forge(Presenter::forge('users'));
        
        $users = Model_Users::find(1,
            array(
                'related' => array('phones')
            )
        );
        $users = Model_Users::query()->get(array(
                'related' => array('phones')
            ));
        $view = View::forge('users/index');
        $view->users = $users;
        //var_dump($users);
        return $view;
        
    }
    
    public function action_autoinsert() {
        $users = Model_Users::forge();
        $users->name ='小久保洋平';
        $users->email = 'hoge@gmail.com';
        $users->sex = 1;
        $users->prefecture_id = 20;
        $users->save();
        
        echo 'ユーザーを追加しました';
    }
}