<?php

/***************************************
phpmyadminあたりで下記のSQLを投げ、ユーザーの電話番号情報を保存する
DBのテーブルを作っておくこと

CREATE TABLE IF NOT EXISTS `telephones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `number` varchar(255) NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DBができたらテーブルに適当な情報をINSERTしておく

INSERT INTO  `myblog`.`telephones` (
`id` ,
`user_id` ,
`number`
)
VALUES (
NULL ,  '1',  '090-1234-5678'
);

****************************************/

class Model_Telephones extends \Orm\Model {
    protected static $_properties = array(
        'id',
        'user_id',
        'number',
    );
}
