<?php

/***************************************
phpmyadminあたりで下記のSQLを投げ、書籍情報を保存する
DBのテーブルを作っておくこと

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` int(11),
  `updated_at` int(11),
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DBができたらテーブルに適当な情報をINSERTしておく

INSERT INTO  `myblog`.`books` (
`id` ,
`title`
)
VALUES (
NULL ,  '銀河鉄道の夜'
);

****************************************/

class Model_Books extends \Orm\Model {
    protected static $_table_name = 'books';
    protected static $_properties = array(
        'id',
        'title',
        'created_at',
        'updated_at',
    );
    
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
}
