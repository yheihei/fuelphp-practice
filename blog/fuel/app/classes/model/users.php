<?php

/***************************************
phpmyadminあたりで下記のSQLを投げ、ユーザー情報を保存する
DBのテーブルを作っておくこと

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sex` int(10) unsigned NOT NULL,
  `prefecture_id` int(11) NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DBができたらテーブルに適当な情報をINSERTしておく

INSERT INTO  `myblog`.`users` (
`id` ,
`name` ,
`email` ,
`sex`,
`prefecture_id`
)
VALUES (
NULL ,  'yohei kokubo',  'yheihei0126@gmail.com',  '1', '42'
);

****************************************/

/*
class Model_Users extends Model_Crud {
    protected static $_table_name = 'users';
    //primary keyはデフォルト「id」となっている。特に指定はいらない
    //protected static $_primary_key = 'id';
    
    //validation定義
    protected static $_rules = array(
        'name' => 'required',
        'email' => 'required|valid_email',
    );
    //デフォルト値の入力
    protected static $_defaults = array(
        'sex' => 1,
    );
    
}
*/

class Model_Users extends \Orm\Model {
    protected static $_table_name = 'users';
    protected static $_properties = array(
        'id',
        'name',
        'email',
        'sex',
        'prefecture_id',
    );
    
    protected static $_has_many = array(
        'phones' => array( //リレーション名
            'model_to' => 'Model_Telephones',
            'key_from' => 'id', //どのキーを
            'key_to' => 'user_id', //相手のどのキーと紐付けるか
            'cascade_save' => true, //現在のモデルを更新時にリレーション先も更新
            'cascade_delete' => true, //現在のモデルを削除時にリレーション先も削除
        ),    
    );
}
