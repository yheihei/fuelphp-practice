<?php

class Presenter_Users extends Presenter {
    private $sexes = array(0 => '未選択', 1 => '男性', 2 => '女性');
    
    public function view() {
        $users = Model_Users::find_all();
        array_walk($users, array($this, "_presentational"));
        $this->users = $users;
    }
    
    private function _presentational(&$data) {
        // 0未選択 1男性 2女性に変換している
        $data->sex_string = $this->sexes[$data->sex];   
    }
}