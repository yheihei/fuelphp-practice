<?php
namespace User; //モジュール内のクラスにはモジュール名と同じ名前空間をつける決まり
class Controller_Hello extends \Controller {
    function action_index() {
        //return \Response::forge('Hello!');
        // 現在要求されたオブジェクトと自コントローラーが実行しているオブジェクトが同じであればブラウザからの要求
        if(\Request::main() === \Request::active()) {
            //ブラウザから呼び出される通常のリクエストの場合
            return \Response::forge('Hello! Browser!');
        } else {
            //HMVCリクエストの場合
            return \Response::forge('Hello! HMVC!');
        }
    }
}
