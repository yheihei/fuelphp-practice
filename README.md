# FuelPHPの練習用リポジトリ #

## FuelPHPの学習に最適 ##

コミットログを古いものから遡って見ることで、
FuelPHPのある程度のことが習得できるように作成しました。

[コミットログ一覧はこちら](https://bitbucket.org/yheihei/fuelphp-practice/commits/all)

## セットアップ方法 ##


```
git clone https://yheihei@bitbucket.org/yheihei/fuelphp-practice.git
cd fuelphp-practice/
cd blog (もしくは他のフォルダ)
./composer.phar install
php oil refine install
```


その後、
```
http://<環境に合わせたURL>/fuelphp-practice/blog/public/
```
にアクセスし、何かしら出ればセットアップは完了です。