<?php
return array(
  'version' => 
  array(
    'app' => 
    array(
      'default' => 
      array(
        0 => '001_create_articles',
        1 => '002_create_users',
        2 => '003_create_comments',
        3 => '004_create_categories',
        4 => '005_create_article_category',
      ),
    ),
    'module' => 
    array(
    ),
    'package' => 
    array(
    ),
  ),
  'folder' => 'migrations/',
  'table' => 'migration',
);
