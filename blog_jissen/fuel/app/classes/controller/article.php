<?php

class Controller_Article extends Controller_Template {
    
    private $per_page = 4;
    
    public function action_index() {
        //ビューに渡す配列の初期化
        $data = array();
        
        //var_dump($data['articles']);
        
        //ページネーションの設定
        $count = Model_Article::count();
        $config = array(
            'pagination_url' => Uri::base(false).'article',
            'uri_segment' => 2,
            'num_links' => 4,
            'per_page' => $this->per_page,
            'total_items' => $count,
        );
        $pagination = Pagination::forge('article_pagination', $config);
        
        //モデルから記事の取得
        $data['articles'] = Model_Article::query()
            ->order_by('created_at', 'desc')
            ->limit($this->per_page)
            ->offset($pagination->offset)
            ->get();
        
        $this->template->title = '記事一覧';
        $this->template->content = View::forge('article/list', $data);
        $this->template->content->set_safe('pagination', $pagination);
    }
    
    public function action_view($id = 0) {
        //ビューに渡す配列の初期化
        $data = array();
        
        //IDが指定されていない場合は一覧にリダイレクト
        if(!$id) Response::redirect('article');
        
        //指定されたIDの記事が見つからない場合は一覧にリダイレクト
        $data['article'] = Model_Article::find($id);
        if (!$data['article']) {
            Response::redirect('article');
        }
        
        //var_dump($data['articles']);
        
        //ビューの読み込み
        $this->template->title = $data['article']->title;
        $this->template->content = View::forge('article/view', $data);
    }
    
    public function action_add() {
        //Model_Articleインスタンスの生成
        $article = Model_Article::forge();
        //Fieldsetインスタンスの生成で記事入力フォーム生成
        $article_form = Fieldset::forge('article_form');
        $article_form->add_model($article);
        $article_form->add('submit', '', array('type'=>'submit', 'value' => '投稿する'));
        
        
        $this->template->title = '記事を書く';
        
        //ビューにフォームのhtmlを受け渡す
        $view = View::forge('article/add');
        $view->set('article_form', $article_form->build('article/submit'), false); //post先のurlを忘れずに
        $this->template->content = $view;
    }
    
    public function action_submit() {
        // formの入力値の確認
        $form = Input::post();
        
        $art = Model_Article::forge();
        $art->user_id = 1;
        $art->title = $form['title'];
        $art->body = $form['body'];
        $art->save();
        
        $this->template->title = '記事が投稿されました';
        $view = View::forge('article/submit');
        $this->template->content = $view;
    }
}